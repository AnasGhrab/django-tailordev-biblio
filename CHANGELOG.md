# TailorDev Biblio

## 0.3 (February 3, 2015)

* Add publication list partial template

## 0.2 (June 2, 2014)

* Add support for partial publication date

### 0.2.1 (February 3, 2015)

* Entry first and last author are now object properties

## 0.1 (January 10, 2014)

First public release. Main features are:

* basic bibliography management
* bibtex file import
* django users/authors linking
* reference list view filtering by author, date, collections

TODO:

* Work on the documentation
* Add EndNote support

