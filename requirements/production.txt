# Install everything that needs to be compiled
-r compiled.txt

# Django
Django>=1.5

# Migrations
South

# Reference manager file parser
bibtexparser
